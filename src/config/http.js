import axios from "axios";

const urlDefault = "http://localhost:3001";
// const urlDefault = process.env.REACT_APP_API;

const http = axios.create({
  baseURL: urlDefault,
});

export default http;
