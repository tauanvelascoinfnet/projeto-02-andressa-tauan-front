import { navigate } from "@reach/router";
import { useEffect, useState } from "react";
import { Container } from "reactstrap";
import styled from "styled-components";
import CardPacote from "../../components/portal/pacotes/cardPacote";
import { obterPacotes } from "../../services/pacotes.service";
// import { type } from 'ramda';

const Pacotes = () => {
  const [pacotes, setPacotes] = useState([]);

  useEffect(() => {
    const get = async () => {
      const resposta = await obterPacotes();
      setPacotes(resposta.data);
    };

    get();
  }, []);

  const vaParaDetalhes = (id) => navigate(`/pacotes/${id}`);
  
  return (
    <Container>
      <PacoteGrid>
        {pacotes.map((pacote) => (
          <CardPacote key={pacote.id} pacote={pacote} handle={vaParaDetalhes} />
        ))}
      </PacoteGrid>
    </Container>
  );
};
export default Pacotes;

const PacoteGrid = styled.div`
  display: grid;
  grid-column-gap: 5px;
  grid-template-columns: 1fr 1fr 1fr 1fr;

  .grid-item {
    background-color: red;
    margin: 5px;
  }
`;
