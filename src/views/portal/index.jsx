import { Router } from "@reach/router";

import Layout from "../../components/portal/layout";
import Home from "./home";
import Pacotes from "./pacotes";
import PacoteDetalhe from "./PacoteDetalhe";
const Portal = () => {
  return (
    <Layout>
      <Router>
        <Home path="/" />
        <Pacotes path="/pacotes" />
        <PacoteDetalhe path="/pacotes/:id" />
      </Router>
    </Layout>
  );
};

export default Portal;
