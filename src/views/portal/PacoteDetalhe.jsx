import { useEffect, useState } from "react";
import { navigate } from "@reach/router";
import {
  inscricaoPacote,
  obterPacotesPorId,
  removerInscricao,
} from "../../services/pacotes.service";
import { Button, Container, Jumbotron } from "reactstrap";
import { toast } from "react-toastify";
import Form from "../../components/portal/pacotes/form";
import TableList from "../../components/portal/pacotes/lista";
import styled from "styled-components";

const PacoteDetalhe = ({ id }) => {
  const [data, setData] = useState({});
  const [isValid, setValid] = useState(false);
  const [hasForm, setHasForm] = useState(false);
  const [inscricoes, setInscricoes] = useState([]);
  const [update, setUpdate] = useState(false);

  useEffect(() => {
    obterPacotesPorId(parseInt(id))
      .then((result) => {
        setData(result.data);
        setValid(true);
      })
      .catch((err) => {
        toast.error("Não existe o Pacote informado...");
        navigate(`/pacotes/`);
      });
  }, [id]);

  useEffect(() => {
    const get = async () => {
      const result = await obterPacotesPorId(id);
      console.log(result);
      setInscricoes(result.data.inscricoes);
    };
    if (!update) {
      get();
    }

    setUpdate(false);
  }, [id, update]);

  if (!isValid) {
    return <div />;
  }

  const toggleForm = () => setHasForm(!hasForm);

  const handleRemove = (id) => {
    removerInscricao(parseInt(id))
      .then(() => {
        toast.success("Inscrição removida com sucesso");
        setUpdate(true);
      })
      .catch(() => toast.error("Erro ao processar a requisição"));
  };

  const submitForm = (form) => {
    inscricaoPacote(id, form)
      .then(() => {
        toast.success(`Inscricao de ${form.nome} feita com sucesso`);
        setHasForm(false);
        setUpdate(true);
      })
      .catch(() => toast.error("Erro ao fazer a inscricao"));
  };

  return (
    <SDivGeral>
      <Container>
        <Jumbotron>
          <SdivCima>
            <SDivCima1>
            <img className="imgPacote" src={data.url_imagem}></img>
            </SDivCima1>
            <SDivCima2>
              {data.inclui_passagem && !data.inclui_hotel && !data.inclui_transfer ? "AÉREO" : ""}
              {!data.inclui_passagem && data.inclui_hotel && !data.inclui_transfer ? "HOSPEDAGEM" : ""}
              {!data.inclui_passagem && !data.inclui_hotel && data.inclui_transfer ? "TRANSFER" : ""}
              {data.inclui_passagem && data.inclui_hotel && data.inclui_transfer ? "AÉREO + HOSPEDAGEM + TRANSFER": ""}
              {data.inclui_passagem && data.inclui_hotel && !data.inclui_transfer ? "AÉREO + HOSPEDAGEM" : ""}
              {data.inclui_passagem && !data.inclui_hotel && !data.inclui_transfer ? "AÉREO + TRANSFER" : ""}
              {!data.inclui_passagem && data.inclui_hotel && data.inclui_transfer ? "HOSPEDAGEM + TRANSFER" : ""}

              <h1 className="titulo">{data.nome}</h1>
              {data.descricao}
            </SDivCima2>
          </SdivCima>
          <SDivBaixo>
            {data.duracao} diárias por apenas: <h3 className="preco"> R${data.valor} </h3>
            Ida: {data.data_ida} Volta: {data.data_volta}
           <br/>
           <SDiv>
            <Button className="botaoInscrever" color="primary" onClick={toggleForm}>
              {hasForm ? "Exibir Lista" : "Inscreva-se"}
            </Button>
           </SDiv>
          </SDivBaixo>
          <Content>
            {hasForm ? (
              <Form
                submit={submitForm}
              /> /* passando por função por parâmetro, pois ela não deve ficar dentro do form.jsx*/
            ) : (
              <TableList handleRemove={handleRemove} inscricoes={inscricoes} />
            )}
          </Content>
        </Jumbotron>
      </Container>
    </SDivGeral>
  );
};

export default PacoteDetalhe;

const Content = styled.div`
  padding: 10px;
`;

const SDivGeral = styled.div`
  text-align: center;
`;

const SDiv = styled.div`
  padding: 10px 0;

  .botaoInscrever{
    border: none;
    background-color: #00BFFF;
  }
`;

const SdivCima = styled.div`
  display: flex;
  itens-align: center;
`;

const SDivCima1 = styled.div`
  width: 193px;
  height: 150px;
  background-color: #ffbf00;
  display: inline-block;
  .imgPacote {
    width: 193px;
    height: 150px;
  }
`;

const SDivCima2 = styled.div`
  width: 100%;
  padding: 15px;
  height: 150px;
  background-color: #ffbf00;

  display: inline-block;

  .titulo{
    color: white;
  }
`;

const SDivBaixo = styled.div`
  width: 100%;
  height: 140px;
  background-color: white;

  .preco{
    color: #32cd32;
  }
`;
