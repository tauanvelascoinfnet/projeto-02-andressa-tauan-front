import { Container } from "reactstrap";
import Banner from "../../components/portal/banner/index";

const Home = () => {
  return (
    <Container>
      <Banner />
    </Container>
  );
};

export default Home;
