import React from "react";
import { Jumbotron, Button, NavbarBrand } from "reactstrap";
import styled from "styled-components";
import ImgLogo from "../../../assets/img/banner.jpg";

const BannerContainer = (props) => {
  return (
    <div>
      <Banner>
        <h1 className="display-6">Viagem é com a Let's Travel!</h1>
        <Logo>
          <img src={ImgLogo}></img>
        </Logo>
        {/* <p className="lead">Selecione seu pacote agora mesmo!</p> */}
        <hr className="my-2" />
        <p>As melhores excursões, todas em PROMOÇÃO!</p>
        <p className="lead">
          <Button color="primary">PACOTES</Button>
        </p>
      </Banner>
    </div>
  );
};

export default BannerContainer;

const Banner = styled(Jumbotron)`
  padding: 10px;
  background-color: #F7E9E9;
  margin: 10px;
  text-align: center;
`;

const Logo = styled(NavbarBrand)`
  img {
    border: thin black;
    max-height: 300px;
  }
`;
