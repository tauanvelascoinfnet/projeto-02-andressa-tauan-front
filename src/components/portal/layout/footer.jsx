import styled from "styled-components";

const FooterContainer = () => {
  return (
    <Footer className="d-none d-md-block">
      Os melhores preços, com as melhores condições!
    </Footer>
  );
};

export default FooterContainer;

const Footer = styled.footer`
  background-color: deepSkyBlue;
  text-align: center;
  color: #fff;
  padding: 5px;
`;

// ## advanced
// const Footer = styled.footer.attrs({
//   className: "d-none d-md-block",
// })`
//   background-color: #f39c12;
// `;
