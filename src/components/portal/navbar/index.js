import React, { useState } from "react";
import {
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container,
  Collapse,
} from "reactstrap";
import { Link } from "@reach/router";
import styled from "styled-components";
import { sizeDevice } from "../../../util/devices";
import ImgLogo from "../../../assets/img/logo.png";
const NavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Navbar light expand="md">
      <Container>
        <Logo href="/">
          <img src={ImgLogo} alt="Let's Travel Excursões" />
        </Logo>
        <NavbarToggler onClick={toggle} />
        <Menu isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <SNavItem>
              <NavLink tag={Link} to="/">
                Home
              </NavLink>
            </SNavItem>
            <SNavItem>
              <NavLink tag={Link} to="/pacotes">
                Pacotes
              </NavLink>
            </SNavItem>
          </Nav>
        </Menu>
      </Container>
    </Navbar>
  );
};

export default NavBar;

const Logo = styled(NavbarBrand)`
  flex: 1;

  img {
    max-height: 70px;
  }
`;

const Menu = styled(Collapse)`
  @media (min-width: ${sizeDevice.tablet}) {
    flex: 0;
  }
`;

const SNavItem = styled(NavItem)`
  background-color: #fff;
  border: 1px thin;
  border-radius: 10px;
  margin: auto 5px;
  &:hover {
    background-color: lightblue;
`;
