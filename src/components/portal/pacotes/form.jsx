import React, { useState } from "react";
import { Button, FormGroup, Label, Input } from "reactstrap";
import styled from "styled-components";

const FormSubscription = ({ submit }) => {
  const [form, setForm] = useState({});

  const handleChange = (event) => {
    const { name, value } = event.target;

    setForm({
      ...form,
      [name]: value,
    });
  };

  const isValidForm = () =>
    form.nome?.length > 0 && // igual a form.nome && form.nome.length > 0
    form.email?.length > 0 &&
    form.datanascimento?.length > 0 &&
    form.telefone?.length > 0;

  return (
    <Form>
      <div className="title">Formulário de inscrição</div>
      <FormGroup>
        <Label for="nome">Nome:</Label>
        <Input
          type="text"
          name="nome"
          onChange={handleChange}
          placeholder="Informe seu nome"
          value={form.nome || ""}
        />
      </FormGroup>
      <FormGroup>
        <Label for="email">Email:</Label>
        <Input
          type="email"
          name="email"
          onChange={handleChange}
          placeholder="Informe seu email"
          value={form.email || ""}
        />
      </FormGroup>
      <FormGroup>
        <Label for="telefone">Telefone:</Label>
        <Input
          type="tel"
          name="telefone"
          onChange={handleChange}
          placeholder="Informe seu telefone"
          value={form.telefone || ""}
        />
      </FormGroup>
      <FormGroup>
        <Label for="datanascimento">Data de Nascimento:</Label>
        <Input
          type="date"
          name="datanascimento"
          onChange={handleChange}
          placeholder="Informe seu nascimento"
          value={form.datanascimento || ""}
        />
      </FormGroup>

      <Button
        color={isValidForm() ? "primary" : "secondary"}
        disabled={!isValidForm()}
        size="sm"
        onClick={() => submit(form)}
      >
        Enviar
      </Button>
    </Form>
  );
};

export default FormSubscription;

const Form = styled.div`
  padding: 10px;
  background-color: #fafafa;
  border: thin solid #ddd;
  margin: 20px 0;

  .title {
    padding: 10px 5px;
    background-color: #ddd;
    color: #222;
    margin-bottom: 10px;
    text-align: center;
  }

  .form-group {
    margin: 15px auto;
  }
`;