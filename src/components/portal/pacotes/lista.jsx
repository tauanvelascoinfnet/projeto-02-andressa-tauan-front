import { Alert, Table } from "reactstrap";
import { FiAlertTriangle, FiTrash2 } from "react-icons/fi";
import styled from "styled-components";

const TableList = ({ handleRemove, inscricoes }) => {

  if (inscricoes.length === 0) {
    return (
      <Alert color="info">
        {" "}
        <FiAlertTriangle style={{ fontSize: "20px" }} /> Ainda não existem inscritos
        nesta excursão.{" "}
      </Alert>
    );
  }

  return (
    <Table striped>
      <thead>
        <tr>
          <th width="5%">id</th>
          <th width="35%">Nome</th>
          <th width="35%">Email</th>
          <th width="20%">Telefone</th>
          <th width="5%">Remover</th>
        </tr>
      </thead>
      <tbody>
        {inscricoes.map(({ id, nome, email, telefone }, i) => (
          <tr key={i}>
            <td>{id}</td>
            <td>{nome}</td>
            <td>{email}</td>
            <td>{telefone}</td>
            <td className="text-center">
              <RemoverInscricao onClick={() => handleRemove(id)} />
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};
export default TableList;

const RemoverInscricao = styled(FiTrash2)`
  cursor: pointer;
  font-size: 20px;
  :hover {
    color: red;
  }
`;
