import React from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
} from "reactstrap";
import styled from "styled-components";

const CardPacote = ({ pacote, handle }) => {
  const { id, nome, descricao, valor, url_imagem } = pacote;
  return (
    <SCard>
      <SCardImg top width="100%" src={url_imagem} alt="Card image cap" />
      <CardBody>
        <CardTitle tag="h5"> {nome}</CardTitle>
        <CardSubtitle tag="h6" className="mb-2 text-muted">
          Valor: R${valor}
        </CardSubtitle>
        <CardText>{descricao}</CardText>
        <Button size="sm" color="primary" onClick={() => handle(id)}>
          Inscreva-se
        </Button>
      </CardBody>
    </SCard>
  );
};

export default CardPacote;

const SCard = styled(Card)`
  margin: 10px;
`;

const SCardImg = styled(CardImg)`
  height: 150px;
`;
