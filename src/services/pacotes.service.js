import http from "../config/http";

const pathDefault = "/pacotes";

export const obterPacotes = () => http.get(pathDefault);

export const obterPacotesPorId = (id) => http.get(`${pathDefault}/${id}`);

export const inscricaoPacote = (idPacote, data) => http.post(`pacotes/${idPacote}/inscricoes`, data);

export const removerInscricao = (id) => http.delete(`inscricoes/${id}`);