import React from "react";
import ReactDOM from "react-dom";
import Routers from "./routers.js";
import "./config/starter";
import GlobalStyled from "./assets/globalstyled";
import ToastContainer from "./plugins/toast";

ReactDOM.render(
  <React.Fragment>
    <>
      <Routers />
      <GlobalStyled />
      <ToastContainer />
    </>
  </React.Fragment>,
  document.getElementById("root")
);
