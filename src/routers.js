import { Router } from "@reach/router";
//errors
import Portal from "./views/portal";

const Routers = () => (
  <Router>
    <Portal path="/*" />
  </Router>
);

export default Routers;
